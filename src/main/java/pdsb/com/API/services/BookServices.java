package pdsb.com.API.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.Data;
import pdsb.com.API.models.book;
import pdsb.com.API.repositorys.BookRepository;

@Data
@Service
public class BookServices {
    
    @Autowired
    private BookRepository bookRepository;

    public Iterable<book> getAllBooks(){
        return bookRepository.findAll();
    }

    public book getBook(Long id) {
        return bookRepository.findById(id).orElse(null);
    }

    public book findBookByTitle(String title) {
        return bookRepository.findByTitle(title);
    }

    public void deleteBook(final Long id){
        bookRepository.deleteById(id);
    }
    public book updateBook(book book){
        book savedBook = bookRepository.save(book);
        return savedBook;
    }

    public book createBook(book book) {
        return bookRepository.save(book);
    }
}
