package pdsb.com.API.repositorys;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pdsb.com.API.models.book;

@Repository
public interface BookRepository extends CrudRepository<book, Long>{
    book findByTitle(String title);
}  