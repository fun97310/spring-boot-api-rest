package pdsb.com.API.models;
import org.springframework.beans.factory.annotation.Value;

/*Crée une entité Book qui contient les attributs : 
id (Long, clé primaire auto incrémentée), 
title (String, non null, longueur maximale de 100 caractères),
description (String, peut être null, est un texte long),
available (Boolean, défaut à true)*/
import jakarta.persistence.*;
import jakarta.validation.constraints.*;

@Entity
@Table(name= "books")
public class book {

    @Id 
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    @Size(max = 100)
    private String title; 

    @Column(columnDefinition = "TEXT")
    private String description;

    @Value("true")
    private boolean available;

    private book(){ }
    
    public book(String title, String description, boolean available){
        this.title = title;
        this.description = description;
        this.available = available;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
