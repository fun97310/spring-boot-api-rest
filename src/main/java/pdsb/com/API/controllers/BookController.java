package pdsb.com.API.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import pdsb.com.API.models.book;
import pdsb.com.API.services.BookServices;

@RestController
@RequestMapping("books")
public class BookController {

    @Autowired
    private BookServices bookServices;

    @GetMapping
    public Iterable<book> getAllBook(){
        return bookServices.getAllBooks();
    }

    @GetMapping("/{id}")
    public book getBookById(@PathVariable Long id) {
        book book = bookServices.getBook(id);
        if (book == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "objet introuvable");
        }
        return book;
    }

    @GetMapping("/title/{title}")
    public ResponseEntity<book> getBookByTitle(@PathVariable String title) {
        book book = bookServices.findBookByTitle(title);
        if (book != null) {
            return ResponseEntity.ok(book);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<book> createBook(@RequestBody book book) {
        book createdBook = bookServices.createBook(book);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdBook);
    }

    @PutMapping("/{id}")
    public ResponseEntity<book> updateBook(@PathVariable Long id, @RequestBody book updatedBook) {
        book existingBook = bookServices.getBook(id);
        if (existingBook == null) {
            return ResponseEntity.notFound().build();
        }
        
        existingBook.setTitle(updatedBook.getTitle());
        existingBook.setDescription(updatedBook.getDescription());
        existingBook.setAvailable(updatedBook.isAvailable());

        book savedBook = bookServices.updateBook(existingBook);
        return ResponseEntity.ok(savedBook);
    }

    @DeleteMapping("/{id}")
    public void deleteBook(@PathVariable Long id){
        bookServices.deleteBook(id);
    }
}
